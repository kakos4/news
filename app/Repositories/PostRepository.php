<?php

namespace App\Repositories;

use App\Models\Post;

class PostRepository{
    /**
     * @var Post
     */
    protected $post;

    /**
     * @param Post $post
     */
    public function __construct(Post $post){
        $this->post = $post;
    }

    /**
     * $param $data
     * $return Post
     */
    public function save($data){
        $post = new $this->post;

        $post->title = $data['title'];
        $post->description = $data['description'];

        $post->save();

        return $post->fresh();
    }
}
